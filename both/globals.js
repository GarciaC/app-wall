Collections = {};

var imageStore = new FS.Store.GridFS("images");

Collections.Images = new FS.Collection("images", {
    stores: [imageStore]
});

Collections.Appwall = new Mongo.Collection("appwall");