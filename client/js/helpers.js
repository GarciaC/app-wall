Template.posts.helpers({
    postsList: function () {
        return Collections.Appwall.find({});
    },

    updateMasonry: function () {
        $('.grid').imagesLoaded().done(function () {
            $('.grid').masonry({
                itemSelector: '.grid-item',
                columnWidth: '.grid-sizer',
                gutter: 20
            });
        });
    },

    imageIs: function (imageId) {
        var image = Collections.Images.find({
            _id: imageId
        }).fetch();
        return image[0].url();
    }
});

Accounts.ui.config({
    passwordSignupFields: "USERNAME_ONLY"
});