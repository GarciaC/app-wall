Template.form.events({
    'submit .add-new-post': function (event) {
        event.preventDefault();

        var postImage = event.currentTarget.children[0].children[0].children[1].files[0];
        var message = event.currentTarget.children[1].firstElementChild.value;

        Collections.Images.insert(postImage, function (error, fileObject) {
            if (error) {

            } else {
                Collections.Appwall.insert({
                    message: message,
                    createdAt: new Date(),
                    owner: Meteor.userId(),
                    username: Meteor.user().username,
                    imageId: fileObject._id
                });
            }
            event.currentTarget.children[0].children[0].children[1].files[0] = "";
            event.currentTarget.children[0].firstElementChild.value = "";
            
            $('.grid').masonry('reloadItems');
        });
    }
});